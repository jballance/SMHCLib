/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/Lib/SMHCLib/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2021 RISC OS Developments Ltd.  All rights reserved.
 * Use is subject to license terms.
 */

#define VARIANT A64

/** Max bytes per descriptor
 *  Empirically, 64k/descriptor seems the max that works, but the documentation
 *  says bits 15:0 of the buffer size are valid, so it's safer to stick to 32k
 */
#define MAX_BUFFER_BYTES 0x8000

/* Register layout */

#define SMHC_CTRL     (0x00)
#define SMHC_TMOUT    (0x08)
#define SMHC_CTYPE    (0x0c)
#define SMHC_BLKSIZ   (0x10)
#define SMHC_BYTCNT   (0x14)
#define SMHC_CMD      (0x18)
#define SMHC_CMDARG   (0x1c)
#define SMHC_RESP0    (0x20)
#define SMHC_RESP1    (0x24)
#define SMHC_RESP2    (0x28)
#define SMHC_RESP3    (0x2c)
#define SMHC_INTMASK  (0x30)
#define SMHC_MINTSTS  (0x34)
#define SMHC_RINTSTS  (0x38)
#define SMHC_STATUS   (0x3c)
#define SMHC_FIFOTH   (0x40)
#define SMHC_TBC1     (0x4c)
#define SMHC_A12A     (0x58)
#define SMHC_BUS_MODE (0x80)
#undef  SMHC_POLLDMND
#define SMHC_DLBA     (0x84)
#define SMHC_IDST     (0x88)
#define SMHC_IDIE     (0x8c)
#define SMHC_THLD     (0x100)

/* Bitfield definitions that vary between controllers */

// bits in SMHC_CTRL
#define USE_INT_DMA     0
#define TIME_UNIT_DA    (1u<<11)

// bits in SMHC_CTYPE
#define CARD_WIDTH_8    (1u<<1)

// bits in SMHC_CMD
#define USE_HOLD_REG    0

// bits in SMHC_INTMASK, SMHC_MINTSTS and SMHC_RINTSTS
#define INT_SDIO        (1u<<16)

// bits in SMHC_BUS_MODE
#define DES_LOAD_CTRL   (1u<<31)

#include "device.h"
