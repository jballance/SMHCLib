# Makefile for SMHCLib

COMPONENT ?= SMHCLib
OBJS       = a64 rk3399 cachemaint memcpy
CFLAGS     = ${C_NO_STKCHK}
LIBRARIES  = ${LIBRARY}

include CLibrary

# Dynamic dependencies:
